package com.l4digital.fastscroll.example.bean;

public class ListBean {
    private String text;
    private int image;

    /**
     * 构造方法
     *
     * @param aText aText
     */
    public ListBean(String aText) {
        this.text = aText;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String aText) {
        this.text = aText;
    }
}
