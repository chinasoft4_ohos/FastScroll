package com.l4digital.fastscroll.example.bean;

public class IconBean {
    private int imageId;
    private String text;

    /**
     * 构造方法
     *
     * @param mText mText
     * @param aImageId aImageId
     */
    public IconBean(String mText,int aImageId) {
        this.imageId = aImageId;
        this.text = mText;
    }

    public String getText() {
        return text;
    }

    public void setText(String aText) {
        this.text = aText;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int aImageId) {
        this.imageId = aImageId;
    }
}
