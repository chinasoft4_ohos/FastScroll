/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.l4digital.fastscroll.example;

import com.l4digital.fastscroll.example.adapter.SampleItemProvider;
import com.l4digital.fastscroll.fastscroll.FastScroller;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;

public class DateViewCreateHelper {
    private AbilitySlice slice;

    /**
     * 创建实例
     *
     * @param abilitySlice 上下文
     */
    public DateViewCreateHelper(AbilitySlice abilitySlice) {
        this.slice = abilitySlice;
    }

    /**
     * 创建Component
     *
     * @param res 资源文件
     * @param position 位置
     * @return 返回Component
     */
    public Component createView(int res, int position) {
        Component mainComponent = LayoutScatter.getInstance(slice).parse(res, null, false);
        ComponentContainer rootLayout = (ComponentContainer) mainComponent;
        initViewDateActivity(rootLayout, position);
        return rootLayout;
    }

    /**
     * 初始化数据
     *
     * @param mainComponent 布局容器
     * @param position 位置
     */
    public void initViewDateActivity(ComponentContainer mainComponent, int position) {
        // 列表
        ListContainer listContainer = (ListContainer) mainComponent.findComponentById(ResourceTable.Id_list_con);
        FastScroller materiaScroller = (FastScroller) mainComponent.findComponentById(ResourceTable.Id_fast);
        SampleItemProvider itemPro = new SampleItemProvider(getDataArrayList(ResourceTable.Strarray_countries_array), this.slice);
        listContainer.setItemProvider(itemPro);
        listContainer.setDraggedListener(Component.DRAG_HORIZONTAL_VERTICAL, null);
        materiaScroller.setListContainer(listContainer);
    }

    /**
     * 获取资源数据
     *
     * @param res 资源id
     * @return 返回数据集合
     */
    public ArrayList<String> getDataArrayList(int res) {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            String[] result = slice.getResourceManager().getElement(res).getStringArray();
            for (String str : result) {
                arrayList.add(str);
            }
            return arrayList;
        } catch (IOException e) {
        } catch (NotExistException e) {
        } catch (WrongTypeException e) {
        }
        return arrayList;
    }
}
