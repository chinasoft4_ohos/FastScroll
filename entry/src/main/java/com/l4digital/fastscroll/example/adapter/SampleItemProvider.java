/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.l4digital.fastscroll.example.adapter;


import com.l4digital.fastscroll.example.ResourceTable;
import com.l4digital.fastscroll.fastscroll.TitleInterFace;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

public class SampleItemProvider extends BaseItemProvider implements TitleInterFace {
    private List<String> sampleItemList;
    private Context context;
    private boolean isHorizontal;

    /**
     * 构造函数
     *
     * @param sampleItemList 数据集合
     * @param context 上下文
     */
    public SampleItemProvider(List<String> sampleItemList, Context context) {
        this.sampleItemList = sampleItemList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return sampleItemList == null ? 0 : sampleItemList.size();
    }

    @Override
    public Object getItem(int position) {
        if (sampleItemList != null && position >= 0 && position < sampleItemList.size()) {
            return sampleItemList.get(position);
        }
        return "";
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        SampleItemHolder sampleItemHolder;
        String item = sampleItemList.get(i);
        if (component == null) {
            cpt = LayoutScatter.getInstance(context).parse(isHorizontal ? ResourceTable.Layout_item_sample
                    : ResourceTable.Layout_item_sample_horizontal, null, false);
            sampleItemHolder = new SampleItemHolder(cpt);
            cpt.setTag(sampleItemHolder);
        } else {
            cpt = component;
            sampleItemHolder = (SampleItemHolder) cpt.getTag();
        }
        sampleItemHolder.text.setText(item);
        return cpt;
    }

    private String getCountry(int position) {
        return sampleItemList.get(position);
    }

    @Override
    public String getSectionTitle(int position) {
        return getCountry(position).substring(0, 1);
    }

    /**
     * SampleItemHolder
     *
     * @since 2021-04-10
     */
    static class SampleItemHolder {
        private Text text;

        SampleItemHolder(Component component) {
            text = (Text) component;
        }
    }
}
