package com.l4digital.fastscroll.example;

import com.l4digital.fastscroll.example.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.rgb(191, 89, 38));
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
