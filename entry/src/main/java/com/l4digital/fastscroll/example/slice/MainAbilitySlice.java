package com.l4digital.fastscroll.example.slice;

import com.l4digital.fastscroll.example.DateViewCreateHelper;
import com.l4digital.fastscroll.example.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DirectionalLayout;

public class MainAbilitySlice extends AbilitySlice {
    private int[] resLayout = new int[]{ ResourceTable.Layout_page_styled
    };
    private DirectionalLayout directionalLayout;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        directionalLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_one);
        DateViewCreateHelper dateViewCreateHelper = new DateViewCreateHelper(this);
        directionalLayout.addComponent(dateViewCreateHelper.createView(resLayout[0], 2));
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
