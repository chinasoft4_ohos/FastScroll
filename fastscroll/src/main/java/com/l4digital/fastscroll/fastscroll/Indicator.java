/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.l4digital.fastscroll.fastscroll;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;

public class Indicator extends FastScrollRecyclerView {
    protected Component bubble;
    protected Component handle;
    private final static int paddingDefault = 1000;
    private final static float mOffDefault = 2f;
    protected Component handleCom;
    /**
     * 滑动bar大小
     *
     * @param container The container for the view
     * @return handle
     */
    @Override
    public Component provideHandleView(ComponentContainer container) {
        handle = new Image(getContext());
        int verticalInset = 0 ;
        int horizontalInset = !getScroller().isVertical() ? 0 : AttrHelper.vp2px(12, getContext());
        ShapeElement shapeElement = new ShapeElement(getContext(), ResourceTable.Graphic_fastscroll__default_handle);
        ScrollingUtilities.setBackground(handle, shapeElement);
        int handleWidth = 13;
        int handleHeight = 120 ;
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(handleWidth, handleHeight);
        layoutConfig.setMargins(horizontalInset, verticalInset, horizontalInset, verticalInset);
        handle.setLayoutConfig(layoutConfig);
        return handle;
    }

    /**
     * 滑动背景
     *
     * @param container The container
     * @return handleCom
     */
    @Override
    public Component provideHandleComte(ComponentContainer container) {
        handleCom = new Text(getContext());
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(ShapeElement.RECTANGLE);
        shapeElement.setRgbColor(new RgbColor(255, 255, 255));
        ScrollingUtilities.setBackground(handleCom, shapeElement);
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(40, ComponentContainer.LayoutConfig.MATCH_PARENT);
        handleCom.setLayoutConfig(layoutConfig);
        handleCom.setMarginLeft(30);
        return handleCom;
    }

    /**
     * 自定义滑动bar
     *
     * @param container properly.
     * @return bubble
     */
    @Override
    public Component provideBubbleView(ComponentContainer container) {
        int radius = AttrHelper.vp2px(paddingDefault, getContext());
        float[] radii = new float[]{
            radius, radius,
            radius, radius,
            0, 0,
            radius, radius
        };
        ShapeElement shapeElement2 = new ShapeElement(getContext(), ResourceTable.Graphic_materia_default);
        shapeElement2.setCornerRadiiArray(radii);
        bubble = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_fastscroll_defalut_bubble, container, false);
        bubble.setBackground(shapeElement2);
        bubble.setMarginLeft(15);
        return bubble;
    }

    /**
     * 滑动块文字
     *
     * @return bubble
     */
    @Override
    public Text provideBubbleTextView() {
        return (Text) bubble;
    }

    /**
     * 偏移量
     *
     * @return BubbleOffset
     */
    @Override
    public int getBubbleOffset() {
        return (int) (((float) handle.getHeight()) / mOffDefault - bubble.getHeight()
               );
    }

    /**
     * 滑动bar
     *
     * @return provideHandleBehavior
     */
    @Override
    protected ViewBehavior provideHandleBehavior() {
        return null;
    }

    /**
     * 滑动bar动画
     *
     * @return provideBubbleBehavior
     */
    @Override
    protected ViewBehavior provideBubbleBehavior() {
        return new FastScrollView(new AnimationManager.Builder(bubble)
                .withPivotX(1f).withPivotY(1f).build());
    }
}
