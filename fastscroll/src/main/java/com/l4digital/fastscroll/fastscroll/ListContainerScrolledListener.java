/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.l4digital.fastscroll.fastscroll;

import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

public class ListContainerScrolledListener implements ListContainer.ScrolledListener {
    private FastScroller scrolled;
    private List<ScrolledListener> listeners = new ArrayList<>();
    private int oldScrollState = Component.SCROLL_IDLE_STAGE;

    public List<ScrolledListener> getListeners() {
        return listeners;
    }

    /**
     * 实例化 ListContainerScrolledListener
     *
     * @param aScrolled MateriaScroller
     */
    public ListContainerScrolledListener(FastScroller aScrolled) {
        this.scrolled = aScrolled;
    }

    /**
     * 添加监听
     *
     * @param listener 监听
     */
    public void addScrolledListener(ScrolledListener listener) {
        listeners.add(listener);
    }

    @Override
    public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
        ListContainer listContainer = (ListContainer) component;
        if (scrolled.shouldUpdateHandlePosition()) {
            updateHandlePosition(listContainer);
        }
    }

    @Override
    public void scrolledStageUpdate(Component component, int newScrollState) {
        if (newScrollState == Component.SCROLL_IDLE_STAGE && oldScrollState != Component.SCROLL_IDLE_STAGE) {
            scrolled.getViewProvider().onScrollFinished();
        } else if (newScrollState != Component.SCROLL_IDLE_STAGE && oldScrollState == Component.SCROLL_IDLE_STAGE) {
            scrolled.getViewProvider().onScrollStarted();
        }
        oldScrollState = newScrollState;
    }

    void updateHandlePosition(ListContainer lc) {
        float relativePos;
        int offset = lc.getScrollValue(Component.AXIS_Y);
        int extent = lc.getHeight();
        int range = scrolled.getTotalScrollH();
        relativePos = offset / (float) (range - extent);
        scrolled.setScrolledPosition(relativePos);
        notifyListeners(relativePos);
    }

    /**
     * 通知 ScrolledListener更新
     *
     * @param relativePos item 位置
     */
    public void notifyListeners(float relativePos) {
        for (ScrolledListener listener : listeners) {
            listener.onScroll(relativePos);
        }
    }

    public interface ScrolledListener {
        /**
         * 滚动位置
         *
         * @param relativePos item 位置
         */
        void onScroll(float relativePos);
    }
}
