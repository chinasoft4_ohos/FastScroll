/*
 * Copyright 2018 L4 Digital. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.l4digital.fastscroll.fastscroll;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.app.dispatcher.task.Revocable;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

public class FastScroller extends DirectionalLayout {
    private static final int STYLE_NONE = -1;
    private final ListContainerScrolledListener scrollListener = new ListContainerScrolledListener(this);
    private ListContainer listContainer;
    private Component bubble;
    private Component handle;
    private Text bubbleTextView;
    private int bubbleOffset;
    private Color handleColor;
    private Color bubbleColor;
    private Color bubbleTextColor;
    private int bubbleTextSize;
    private int scrolledOrientation;
    private int totalScrollH = 0;
    private int maxVisibility;
    private boolean isChangingPosition;
    private FastScrollRecyclerView viewProvider;
    private TitleInterFace titleProvider;
    private float offset = 0;
    private int targetPos;
    private Revocable revocable;
    private AnimatorProperty hideAnimator;
    float moveDown = 0;
    float moveUp = 0;
    private int a;
    private DependentLayout dependentLayout;
    private Component handleCom;
    private Color handleComColor;

    /**
     * build
     *
     * @param context context
     */
    public FastScroller(Context context) {
        super(context, null);
    }

    /**
     * 构造方法
     *
     * @param context context
     * @param attrSet attrSet
     */
    public FastScroller(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 构造方法
     *
     * @param context context
     * @param attrSet attrSet
     * @param styleName styleName
     */
    public FastScroller(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setClipEnabled(false);
        initAttrSet(attrSet);
        initArrange();
    }

    /**
     * 初始化属性值
     *
     * @param attrSet attrSet
     */
    private void initAttrSet(AttrSet attrSet) {
        hideAnimator = new AnimatorProperty();
        handleComColor = AttrValue.get(attrSet, "handlecomColor", new Color(STYLE_NONE));
        handleColor = AttrValue.get(attrSet, "handleColor", new Color(STYLE_NONE));
        bubbleColor = AttrValue.get(attrSet, "bubbleColor", new Color(STYLE_NONE));
        bubbleTextColor = AttrValue.get(attrSet, "bubbleTextColor", new Color(STYLE_NONE));
        bubbleTextSize = AttrValue.getDimension(attrSet, "bubbleTextSize", STYLE_NONE);
        maxVisibility = getVisibility();
        setOrientation(getOrientation());
        setViewProvider(new Indicator());
    }

    /**
     * Enables custom layout for {@link FastScroller}.
     *
     * @param aViewProvider A {@link FastScrollRecyclerView} for the {@link FastScroller} to use when building layout.
     */
    public void setViewProvider(FastScrollRecyclerView aViewProvider) {
        removeAllComponents();
        this.viewProvider = aViewProvider;
        viewProvider.setFastScroller(this);
        bubble = viewProvider.provideBubbleView(this);
        handle = viewProvider.provideHandleView(this);
        bubbleTextView = viewProvider.provideBubbleTextView();
        handleCom = viewProvider.provideHandleComte(this);
        dependentLayout = new DependentLayout(getContext());
        dependentLayout.addComponent(handleCom);
        dependentLayout.addComponent(handle);
        addComponent(bubble);
        addComponent(dependentLayout);
        initDrag();
    }

    /**
     * 设置拖拽监听
     */
    private void initDrag() {
        handle.setDraggedListener(Component.DRAG_HORIZONTAL_VERTICAL, new DraggedListener() {
            @Override
            public void onDragDown(Component component, DragInfo dragInfo) {
            }

            @Override
            public void onDragStart(Component component, DragInfo dragInfo) {
            }

            @Override
            public void onDragUpdate(Component component, DragInfo dragInfo) {
            }

            @Override
            public void onDragEnd(Component component, DragInfo dragInfo) {
            }

            @Override
            public void onDragCancel(Component component, DragInfo dragInfo) {
            }

            @Override
            public boolean onDragPreAccept(Component component, int dragDirection) {
                return true;
            }
        });
    }

    /**
     * setListContainer
     *
     * @param aListContainer aListContainer
     */
    public void setListContainer(ListContainer aListContainer) {
        handle.setVisibility(HIDE);
        this.listContainer = aListContainer;
        if (listContainer.getItemProvider() instanceof TitleInterFace) {
            titleProvider = (TitleInterFace) listContainer.getItemProvider();
        }
        listContainer.setScrolledListener(scrollListener);
        invalidateVisibility();
        listContainer.setLayoutRefreshedListener(new LayoutRefreshedListener() {
            @Override
            public void onRefreshed(Component component) {
                Component cp = listContainer.getComponentAt(0);
                if (totalScrollH == 0 && cp != null) {
                    int height = cp.getHeight();
                    totalScrollH = height * listContainer.getItemProvider().getCount();
                }
            }
        });

        listContainer.addScrolledListener(new ScrolledListener() {
            @Override
            public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
                handle.setVisibility(VISIBLE);
            }
        });
        listContainer.setScrollListener(new ListContainer.ScrollListener() {
            @Override
            public void onScrollFinished() {
                initTime();
            }
        });
        listContainer.addItemVisibilityChangedListener(new ListContainer.ItemVisibilityChangedListener() {
            @Override
            public void onItemAdded(Component component, int i) {
                invalidateVisibility();
            }

            @Override
            public void onItemRemoved(Component component, int i) {
                invalidateVisibility();
            }
        });
        listContainer.setTouchEventListener(new TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                int action = touchEvent.getAction();
                if (action == TouchEvent.POINT_MOVE) {
                    handle.setVisibility(VISIBLE);
                    initTime();
                }
                return true;
            }
        });
        listContainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                handle.setVisibility(HIDE);
            }
        });
    }

    private void initTime() {
        if (revocable != null) {
            revocable.revoke();
        }
        revocable = getContext().getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                handle.setVisibility(Component.HIDE);
                hideAnimator.scaleX(0).scaleY(0).alpha(0).setDuration(200).setCurveType(Animator.CurveType.ACCELERATE);
            }
        }, 2000);
    }

    private void initArrange() {
        addDrawTask(new DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                bubbleOffset = viewProvider.getBubbleOffset();
                applyStyling();
            }
        });

        setLayoutRefreshedListener(new LayoutRefreshedListener() {
            @Override
            public void onRefreshed(Component component) {
                invalidateVisibility();
                initDateHandleMovement();
                bubbleOffset = viewProvider.getBubbleOffset();
            }
        });
    }

    /**
     * 获取可滑动的总高度
     *
     * @return 高度总滑动值
     */
    protected int getTotalScrollH() {
        return totalScrollH;
    }

    private void initDateHandleMovement() {
        dependentLayout.setTouchEventListener(new TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent event) {
                MmiPoint pointerPosition = event.getPointerPosition(event.getIndex());
                MmiPoint mScreenPosition = event.getPointerScreenPosition(0);
                switch (event.getAction()) {
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        moveDown = pointerPosition.getY();
                        handle.setVisibility(VISIBLE);
                        ShapeElement shapeElement = new ShapeElement(getContext(), ResourceTable.Graphic_fastscroll__move_handle);
                        ScrollingUtilities.setBackground(handle, shapeElement);
                        if (titleProvider != null) {
                            viewProvider.onHandleGrabbed();
                            float offsetY = mScreenPosition.getY() - handle.getLocationOnScreen()[1];
                            offset = offsetY;
                            return true;
                        }
                        break;
                    case TouchEvent.POINT_MOVE:
                        moveUp = pointerPosition.getY();
                        handle.setVisibility(VISIBLE);
                        ShapeElement shapeElementMove = new ShapeElement(getContext(), ResourceTable.Graphic_fastscroll__move_handle);
                        ScrollingUtilities.setBackground(handle, shapeElementMove);
                        isChangingPosition = true;
                        float relativePos = getRelativeTouchPosition(event);
                        if (relativePos > 1) {
                            relativePos = 1;
                        } else if (relativePos < 0) {
                            relativePos = 0;
                        }
                        if (moveDown > moveUp) {
                            a = 0;
                        } else if (moveDown < moveUp) {
                            a = 1;
                        }
                        setScrolledPosition(relativePos);
                        setRecyclerViewPosition(relativePos);
                        setScrolledPosition(relativePos);
                        break;
                    case TouchEvent.PRIMARY_POINT_UP:
                    case TouchEvent.CANCEL:
                        if (titleProvider != null) {
                            viewProvider.onHandleReleased();
                        }
                        ShapeElement shapeDefaut = new ShapeElement(getContext(), ResourceTable.Graphic_fastscroll__default_handle);
                        ScrollingUtilities.setBackground(handle, shapeDefaut);
                        initTime();
                        isChangingPosition = false;
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }

    private float getRelativeTouchPosition(TouchEvent event) {
        float aParent = event.getPointerScreenPosition(0).getY() - ScrollingUtilities.getViewRawY(handle) - offset;
        return aParent / (getHeight() - handle.getHeight());
    }

    private void setRecyclerViewPosition(float relativePos) {
        if (listContainer == null) {
            return;
        }
        listContainer.scrollTo(0, (int) (relativePos * (getTotalScrollH())));
        setTextData(relativePos);
        scrollListener.notifyListeners(relativePos);
    }

    void setScrolledPosition(float relativePos) {
        this.bubble.setTranslationY(ScrollingUtilities.getValueInRange(0.0F, (float) (this.getHeight() - this.bubble.getHeight()), relativePos * (float) (this.getHeight() - this.handle.getHeight()) + (float) this.bubbleOffset) - (float) this.bubble.getTop());
        this.handle.setTranslationY(ScrollingUtilities.getValueInRange(0.0F, (float) (this.getHeight() - this.handle.getHeight()), relativePos * (float) (this.getHeight() - this.handle.getHeight())) - (float) this.handle.getTop());
        setTextData(relativePos);
    }

    private void setTextData(double relativePos) {
        int itemCount = listContainer.getItemProvider().getCount();
        targetPos = (int) ScrollingUtilities.getValueInRange(0, itemCount - 1, (int) (relativePos * (float) itemCount));
        if (titleProvider != null && bubbleTextView != null) {
            if (a == 0) {
                bubbleTextView.setText(titleProvider.getSectionTitle(targetPos));
            } else if (a == 1) {
                bubbleTextView.setText(titleProvider.getSectionTitle(listContainer.getLastVisibleItemPosition()));
            }
        }
    }

    private void invalidateVisibility() {
        if (listContainer.getItemProvider() == null
                || listContainer.getItemProvider().getCount() == 0
                || isRecyclerViewNotScrollable()
                || maxVisibility != Component.VISIBLE
        ) {
            super.setVisibility(INVISIBLE);
        } else {
            super.setVisibility(VISIBLE);
        }
    }

    private boolean isRecyclerViewNotScrollable() {
        return listContainer.getHeight() >= totalScrollH;
    }

    private void applyStyling() {
        if (bubbleColor.getValue() != STYLE_NONE) {
            setBackgroundTint(bubbleTextView, bubbleColor);
        }
        if (handleComColor.getValue() != STYLE_NONE) {
            setBackgroundTint(handleCom, handleComColor);
        }
        if (handleColor.getValue() != STYLE_NONE) {
            setBackgroundTint(handle, handleColor);
        }
        if (bubbleTextColor.getValue() != STYLE_NONE) {
            bubbleTextView.setTextColor(new Color(Color.rgb(255, 255, 255)));
        }
        if (bubbleTextSize != STYLE_NONE) {
            bubbleTextView.setTextSize(120);
        }
    }

    private void setBackgroundTint(Component view, Color color) {
        final ShapeElement background = view.getBackgroundElement()
                instanceof ShapeElement ? (ShapeElement) view.getBackgroundElement() : null;
        if (background == null) {
            return;
        }
        background.setRgbColor(RgbColor.fromArgbInt(color.getValue()));
        ScrollingUtilities.setBackground(view, background);
    }

    @Override
    public void setVisibility(int visibility) {
        maxVisibility = visibility;
        invalidateVisibility();
    }

    @Override
    public void setOrientation(int orientation) {
        scrolledOrientation = orientation;
        super.setOrientation(orientation == HORIZONTAL ? VERTICAL : HORIZONTAL);
    }

    public boolean isVertical() {
        return scrolledOrientation == VERTICAL;
    }

    boolean shouldUpdateHandlePosition() {
        return handle != null && !isChangingPosition && listContainer.getChildCount() > 0;
    }

    FastScrollRecyclerView getViewProvider() {
        return viewProvider;
    }
}
