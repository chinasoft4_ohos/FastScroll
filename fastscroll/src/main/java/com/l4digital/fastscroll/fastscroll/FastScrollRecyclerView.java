/*
 * Copyright 2018 L4 Digital. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.l4digital.fastscroll.fastscroll;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.app.Context;

public abstract class FastScrollRecyclerView {
    private FastScroller scroller;
    private ViewBehavior handleBehavior;
    private ViewBehavior bubbleBehavior;

    public void setFastScroller(FastScroller materiaScroller) {
        this.scroller = materiaScroller;
    }

    protected Context getContext() {
        return scroller.getContext();
    }

    protected FastScroller getScroller() {
        return scroller;
    }

    /**
     * 设置滚动条
     *
     * @param container The container {@link FastScroller} for the view to inflate properly.
     * @return A view which will be by the {@link FastScroller} used as a handle.
     */
    public abstract Component provideHandleView(ComponentContainer container);

    /**
     * 设置滚动条
     *
     * @param container The container
     * @return A view
     */
    public abstract Component provideHandleComte(ComponentContainer container);

    /**
     * 设置气泡框
     *
     * @param container properly.
     * @return view
     */
    public abstract Component provideBubbleView(ComponentContainer container);

    /**
     * Bubble view
     *
     * @return title.
     */
    public abstract Text provideBubbleTextView();

    /**
     * To offset the position of the bubble relative to the handle
     * the sharp corner of the bubble is
     *
     * @return the position of the bubble in relation to the handle (according to the orientation).
     */
    public abstract int getBubbleOffset();

    /**
     * 滚动条抽象方法
     *
     * @return 定义ViewBehavior
     */
    protected abstract ViewBehavior provideHandleBehavior();

    /**
     * 气泡框抽象方法
     *
     * @return 定义ViewBehavior
     */
    protected abstract ViewBehavior provideBubbleBehavior();

    /**
     * getHandleBehavior
     *
     * @return handleBehavior
     */
    protected ViewBehavior getHandleBehavior() {
        if (handleBehavior == null) {
            handleBehavior = provideHandleBehavior();
        }
        return handleBehavior;
    }

    /**
     * 滚动条抽象方法
     *
     * @return 定义ViewBehavior
     */
    protected ViewBehavior getBubbleBehavior() {
        if (bubbleBehavior == null) {
            bubbleBehavior = provideBubbleBehavior();
        }
        return bubbleBehavior;
    }

    /**
     * onHandleGrabbed
     */
    public void onHandleGrabbed() {
        if (getHandleBehavior() != null) {
            getHandleBehavior().onHandleGrabbed();
        }
        if (getBubbleBehavior() != null) {
            getBubbleBehavior().onHandleGrabbed();
        }
    }

    /**
     * onHandleReleased
     */
    public void onHandleReleased() {
        if (getHandleBehavior() != null) {
            getHandleBehavior().onHandleReleased();
        }
        if (getBubbleBehavior() != null) {
            getBubbleBehavior().onHandleReleased();
        }
    }

    /**
     * onScrollStarted
     */
    public void onScrollStarted() {
        if (getHandleBehavior() != null) {
            getHandleBehavior().onScrollStarted();
        }
        if (getBubbleBehavior() != null) {
            getBubbleBehavior().onScrollStarted();
        }
    }

    /**
     * onScrollFinished
     */
    public void onScrollFinished() {
        if (getHandleBehavior() != null) {
            getHandleBehavior().onScrollFinished();
        }
        if (getBubbleBehavior() != null) {
            getBubbleBehavior().onScrollFinished();
        }
    }
}
