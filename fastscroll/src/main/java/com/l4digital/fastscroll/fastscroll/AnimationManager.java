/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.l4digital.fastscroll.fastscroll;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

public class AnimationManager {
    protected final Component view;

    protected AnimatorProperty hideAnimator;
    protected AnimatorProperty showAnimator;

    private float pivotxRelative;
    private float pivotyRelative;

    /**
     * AnimationManager
     *
     * @param aView Component
     * @param mShowAnimator 显示动画
     * @param mHideAnimator 隐藏动画
     * @param mPivotxRelative Component X中心轴
     * @param mPivotyRelative Component Y中心轴
     * @param hideDelay 动画启动延时
     */
    protected AnimationManager(final Component aView, AnimatorProperty mShowAnimator, AnimatorProperty mHideAnimator, float mPivotxRelative, float mPivotyRelative, int hideDelay) {
        this.view = aView;
        this.pivotxRelative = mPivotxRelative;
        this.pivotyRelative = mPivotyRelative;
        this.hideAnimator = mHideAnimator;
        this.hideAnimator.setDelay(hideDelay);
        this.hideAnimator.setTarget(view);
        this.showAnimator = mShowAnimator;
        this.showAnimator.setTarget(view);
        this.hideAnimator.setStateChangedListener(new Animator.StateChangedListener() {
            boolean isCanceled;

            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onStop(Animator animator) {
                isCanceled = false;
                view.setVisibility(Component.INVISIBLE);
            }

            @Override
            public void onCancel(Animator animator) {
                isCanceled = true;
            }

            @Override
            public void onEnd(Animator animator) {
                if (!isCanceled) {
                    view.setVisibility(Component.INVISIBLE);
                }
                isCanceled = false;
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        updatePivot();
    }

    /**
     * 启动展示动画
     */
    public void show() {
        hideAnimator.cancel();

        if (view.getVisibility() == Component.INVISIBLE) {
            view.setVisibility(Component.VISIBLE);
            updatePivot();
            showAnimator.start();
        }
    }

    /**
     * 启动隐藏动画
     */
    public void hide() {
        updatePivot();
        hideAnimator.start();
    }

    /**
     * 更新控件中心点坐标
     */
    protected void updatePivot() {
        view.setPivotX(pivotxRelative * view.getWidth());
        view.setPivotY(pivotyRelative * view.getHeight());
    }

    public static abstract class AbsBuilder<T extends AnimationManager> {
        protected final Component view;
        protected AnimatorProperty showAnimator;
        protected AnimatorProperty hideAnimator;
        protected int hideDelay = 0;
        protected float pivotX = 0.5f;
        protected float pivotY = 0.5f;

        /**
         * builder
         *
         * @param aview 动画组件
         */
        public AbsBuilder(Component aview) {
            this.view = aview;
            initDefaultAnimator();
        }

        /**
         * 设置显示动画
         *
         * @param showAnimatorResource 显示动画
         * @return this
         */
        public AbsBuilder<T> withShowAnimator(AnimatorProperty showAnimatorResource) {
            this.showAnimator = showAnimatorResource;
            return this;
        }

        /**
         * 设置隐藏动画
         *
         * @param hideAnimatorResource 隐藏动画
         * @return this;
         */
        public AbsBuilder<T> withHideAnimator(AnimatorProperty hideAnimatorResource) {
            this.hideAnimator = hideAnimatorResource;
            return this;
        }

        /**
         * 设置X轴中心点
         *
         * @param mPivotX X轴中心点
         * @return this;
         */
        public AbsBuilder<T> withPivotX(float mPivotX) {
            this.pivotX = mPivotX;
            return this;
        }

        /**
         * 设置Y轴中心点
         *
         * @param mPivotY Y轴中心点
         * @return this;
         */
        public AbsBuilder<T> withPivotY(float mPivotY) {
            this.pivotY = mPivotY;
            return this;
        }

        /**
         * initDefaultAnimator
         */
        public void initDefaultAnimator() {
            showAnimator = new AnimatorProperty().
                    setDuration(200).setCurveType(Animator.CurveType.ACCELERATE);
            hideAnimator = new AnimatorProperty().
                    setDuration(100).setCurveType(Animator.CurveType.ACCELERATE);
        }

        /**
         * build
         *
         * @return build
         */
        public abstract T build();
    }

    public static class Builder extends AbsBuilder<AnimationManager> {
        /**
         * 构造方法
         *
         * @param view view
         */
        public Builder(Component view) {
            super(view);
        }

        /**
         * 设置build
         *
         * @return this
         */
        public AnimationManager build() {
            return new AnimationManager(view, showAnimator, hideAnimator, pivotX, pivotY, hideDelay);
        }
    }
}
