/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.l4digital.fastscroll.fastscroll;

import ohos.agp.components.Component;
import ohos.agp.components.element.Element;

public class ScrollingUtilities {
    private ScrollingUtilities() {
    }

    /**
     * 控件在其整个屏幕上的坐标位置
     *
     * @param view view
     * @return Y轴位置
     */
    public static float getViewRawY(Component view) {
        int[] location = new int[2];
        location[0] = 0;
        location[1] = ((Component) view.getComponentParent()).getLocationOnScreen()[1];
        return location[1];
    }

    /**
     * 控件在其整个屏幕上的坐标位置
     *
     * @param view view
     * @return X轴位置
     */
    public static float getViewRawX(Component view) {
        int[] location = new int[2];
        location[0] = ((Component) view.getComponentParent()).getLocationOnScreen()[0];
        location[1] = 0;
        return location[0];
    }

    /**
     * 获取最小值
     *
     * @param min min
     * @param max max
     * @param value value
     * @return 返回对比值
     */
    public static float getValueInRange(float min, float max, float value) {
        float minimum = Math.max(min, value);
        return Math.min(minimum, max);
    }

    /**
     * 设置背景
     *
     * @param component component
     * @param element element
     */
    public static void setBackground(Component component, Element element) {
        component.setBackground(element);
    }
}
