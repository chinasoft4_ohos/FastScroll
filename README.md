# FastScroll

#### 项目介绍
- 项目名称：FastScroll
- 所属系列：openharmony的第三方组件适配移植
- 功能：FastScroll风格的滚动条和section“气泡”视图，将快速滚动和section索引功能引入到view中。当冒泡视图显示当前可见的section索引时，滚动条提供了快速导航列表的句柄
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6,DevEco Studio 2.2 Beta1
- 基线版本：Release 2.0.1


#### 效果演示
![效果演示](./img/demo.gif)

#### 安装教程
1、在项目根目录下的build.gradle文件中
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在app模块的build.gradle文件中
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:FastScroll:1.0.0')
    ......  
 }
 ```
sdk6,DevEco Studio 2.2 Beta1下项目可直接运行,

如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，

并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1.如何使用
创建一个FastScroller对象，并在布局中引用
 ```
 <com.l4digital.fastscroll.fastscroll.FastScroller
         ohos:id="$+id:fast"
         ohos:height="match_parent"
         ohos:width="match_content"
         ohos:align_parent_right="true"
         ohos:orientation="vertical"
         app:bubbleColor="#ff0099"
         app:bubbleTextColor="#e0e0e0"
         app:bubbleTextSize="24fp"
         app:handleColor="#ff0099"
         app:textStyle="bold"
         >
 
 </com.turingtechnologies.materialscrollbardemo.MateriaScroller>
 ```
2.自定义气泡框，拖拽bar的类Indicator
 ```
     /**
      * 定义滚动条
      */
        @Override
        public Component provideHandleView(ComponentContainer container) {
            handle = new Hand(getContext());
            DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(60, 300);
            handle.setLayoutConfig(layoutConfig);
            handle.setMarginLeft(10);
            return handle;
        }

     /**
      * 定义气泡框
      */
    @Override
    public Component provideBubbleView(ComponentContainer container) {
        int radius = AttrHelper.vp2px(paddingDefault, getContext());
        float[] radii = new float[]{
                radius, radius,
                radius, radius,
                0, 0,
                radius, radius
        };
        ShapeElement shapeElement2 = new ShapeElement(getContext(), ResourceTable.Graphic_materia_default);
        shapeElement2.setCornerRadiiArray(radii);
        bubble = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_fastscroll_defalut_bubble, container, false);
        bubble.setBackground(shapeElement2);
        bubble.setMarginLeft(15);
        return bubble;
    }

 ```
3.FastScroller
定义属性
 ```	
       handleColor = AttrValue.get(attrSet, "handleColor", new Color(STYLE_NONE));
       handleComColor = AttrValue.get(attrSet, "handlecomColor", new Color(STYLE_NONE));
       bubbleColor = AttrValue.get(attrSet, "bubbleColor", new Color(STYLE_NONE));
       bubbleTextColor = AttrValue.get(attrSet, "bubbleTextColor", new Color(STYLE_NONE));
       bubbleTextSize = AttrValue.getDimension(attrSet, "bubbleTextSize", STYLE_NONE);
       textStyle = AttrValue.get(attrSet, "textStyle", "");
 ```
处理滑动事件
 ```
    /**
      * 获取可滑动的总高度
      *
      * @return 高度总滑动值
      */
     protected int getTotalScrollH() {
         return totalScrollH;
     }
 
     private void initHandleMovement() {
         handle.setTouchEventListener(new TouchEventListener() {
             @Override
             public boolean onTouchEvent(Component component, TouchEvent event) {
                 MmiPoint mScreenPosition = event.getPointerScreenPosition(0);
                 switch (event.getAction()) {
                     case TouchEvent.PRIMARY_POINT_DOWN:
                         scrollMax();
                         if (titleProvider != null) {
                             viewProvider.onHandleGrabbed();
                             float offsetY = mScreenPosition.getY() - handle.getLocationOnScreen()[1];
                             offset = offsetY;
                             return true;
                         }
                         break;
                     case TouchEvent.POINT_MOVE:
                         scrollMax();
                         isChangingPosition = true;
                         float relativePos = getRelativeTouchPosition(event);
                         if (relativePos > 1) relativePos = 1;
                         else if (relativePos < 0) relativePos = 0;
                         setScrolledPosition(relativePos);
                         setRecyclerViewPosition(relativePos);
 ```

4.滑动工具类ScrollingUtilities
控件在其整个屏幕上的坐标位置
 ```	  
  public static float getViewRawY(Component view) {
          int[] location = new int[2];
          location[0] = 0;
          location[1] = ((Component) view.getComponentParent()).getLocationOnScreen()[1];
          return location[1];
      }

 ```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异



#### 版本迭代

- 1.0.0

 #### 版权和许可信息
  ```
   Copyright 2018 L4 Digital. All rights reserved.
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
        http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  ```